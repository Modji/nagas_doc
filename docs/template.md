---
sidebar_position: 2
---

# Template

The website relies on an excel file template to display quotations.

Administrators of the site will see custom messages addressed to them if there's any errors in the excel file. A custom message will also be seen if the template has not been uploaded.

## Possible errors

The following causes may generate errors:

- The excel files contains any reference to an external files
- A cell expecting a number returns a sentence. Check cells' formulas.
- A macro needs to be executed

## Updating the Template

Please see the [Administrative Panel](/docs/administration) documentation.
