---
sidebar_position: 3
---

# Administrative Panel

The Administrative Panel also known as backend is where additional steps can be taken by an employee of Nagas Innovation.

## All Quotations

The `All Quotations` page shows all submitted quotations done. Each quotation can be edited or viewed as a pdf.

### Export

An export of all active quotation can be executed. This will export all content seen on this page.

## Change Template

The excel template can be changed on this page. The excel file must not be bigger than 6Mb. This page does not restore any previous excel files uploaded.

When uploading a new template it will be located at `storage > app > private` and it will be renamed automatically to `Template.xlsx`

Previous templates will be archived at `storage > app > private > archived` and the change date will be appended to the name.

## Emails

Upon a user submitting a new quote, they will receive an email with their quotation attached as a pdf. In order to notify other users of a new quote, their emails must be added to the system. This would allow Nagas Innovation to automatically send a copy of any new quote to their providers or themselves.

## Users

Users can be given access to the site by creating them an account and making them active. If a user is not active, they will not have access to the site.

When creating a new user, and you may request them to validate their email by checking the `Send confirmation E-mail` option. This step can be skipped by checking the `E-mail Verified` option
