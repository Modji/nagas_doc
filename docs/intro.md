---
sidebar_position: 1
---

# Introduction

Welcome to the Nagas Innovation documentation site.

## Getting Started

Most of the editables variables will resides within 4 files.

- .env
- config > quotations.php
- resources > lang > en > table.php
- resources > lang > en > formulaires.php

## Expiration days

The `.env` file allows the website to use environment variables. In there, you'll be able to edit the number of days before a quotation expires. `DAYS_TO_EXPIRE=60` as of right now, it is set to 60 days.

### How to edit

Once logged in via SSH and into the website folder

```shell
sudo nano .env
```

Change the number to the desired number and press `Ctrl + X`. Type `y` and press `Enter`

## Form variables

The `quotations.php` file located in the `config` folder contains all fields used in the form. All variables are set via arrays. The **left** side of the Array is the value that will be displayed on the form. The **right** side of it is the value that will be saved onto the database.

Exemple:

```php
'currency' => [
        'OEM' => 'CAN',
        'OEM USD' => 'USD',
        'Haakon' => 'CAN',
        'Scott Springfield' => 'CAN',
        'MAS-HVAC' => 'USD',
        'Annexair' => 'CAN',
        'Innovent' => 'USD',
    ],
```

The values `OEM, OEM USD, Haakoon, etc...` will be shown on the site, while their currencies will be for the database.

## Form labels

Labels used on the form can be edited the `table` and `formulaires` files located in the **lang** folder.

The `table` file stores all the labels used in tables seen in the 'All Quotations' tables.

The `formulaires` file stores all the labels used in the quote form.
